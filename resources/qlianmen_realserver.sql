-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2020 at 05:15 PM
-- Server version: 5.6.47-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qlianmen_realserver`
--

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id` int(255) NOT NULL,
  `uid` varchar(255) CHARACTER SET latin1 NOT NULL,
  `referral_bonus` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `conversion_point` int(11) NOT NULL,
  `charges_withdraw` int(11) NOT NULL,
  `point_voucher` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id`, `uid`, `referral_bonus`, `commission`, `conversion_point`, `charges_withdraw`, `point_voucher`) VALUES
(1, '', 20, 20, 20, 10, 100);

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(7, 'e809ed5b38535157bf9a163fa1225ade', 'a88b0fa765f0307f4cceec6e6dc37875', 'comp', 1, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-31 02:44:06', '2019-12-31 02:44:06'),
(10, 'a88b0fa765f0307f4cceec6e6dc37875', '956d4c4f9e38455e5ee9a27fe120df52', 'abc', 2, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-31 03:09:11', '2019-12-31 03:09:11'),
(11, '956d4c4f9e38455e5ee9a27fe120df52', '299d7e404a446ae1802106b127cb48f8', 'bbc', 3, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-31 03:13:02', '2019-12-31 03:13:02'),
(12, '299d7e404a446ae1802106b127cb48f8', '1572ebd58677e36834d515bf905dea81', 'Ccc', 4, 'e809ed5b38535157bf9a163fa1225ade', '2019-12-31 12:35:21', '2019-12-31 12:35:21'),
(13, 'a88b0fa765f0307f4cceec6e6dc37875', 'fc8c4a01eaa112adc5071fdfceb09fc4', 'mike123', 2, 'e809ed5b38535157bf9a163fa1225ade', '2020-01-08 02:35:39', '2020-01-08 02:35:39'),
(14, 'a88b0fa765f0307f4cceec6e6dc37875', 'ac2666dcd4e996265cc03e1f9f4984ed', 'mimi', 2, 'e809ed5b38535157bf9a163fa1225ade', '2020-01-08 02:40:19', '2020-01-08 02:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `register_point`
--

CREATE TABLE `register_point` (
  `id` int(11) NOT NULL,
  `send_uid` varchar(100) NOT NULL,
  `send_name` varchar(100) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `receive_name` varchar(100) NOT NULL,
  `receive_uid` varchar(100) NOT NULL,
  `create_date` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `register_point`
--

INSERT INTO `register_point` (`id`, `send_uid`, `send_name`, `amount`, `receive_name`, `receive_uid`, `create_date`, `status`) VALUES
(3, 'e809ed5b38535157bf9a163fa1225ade', 'admin', '10000', 'comp', 'a88b0fa765f0307f4cceec6e6dc37875', '2019-12-31 02:45:08.688', 'RECEIVED'),
(7, 'a88b0fa765f0307f4cceec6e6dc37875', 'comp', '1980', 'abc', '956d4c4f9e38455e5ee9a27fe120df52', '2019-12-31 03:12:46.158', 'RECEIVED'),
(8, 'a88b0fa765f0307f4cceec6e6dc37875', 'comp', '10000', 'abc', '956d4c4f9e38455e5ee9a27fe120df52', '2019-12-31 03:21:09.162', 'RECEIVED'),
(9, '956d4c4f9e38455e5ee9a27fe120df52', 'abc', '10000', 'bbc', '299d7e404a446ae1802106b127cb48f8', '2019-12-31 03:21:28.452', 'RECEIVED');

-- --------------------------------------------------------

--
-- Table structure for table `signup_commission`
--

CREATE TABLE `signup_commission` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referrer_name` varchar(255) DEFAULT NULL,
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `referral_fullname` varchar(255) NOT NULL,
  `commission` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `signup_commission`
--

INSERT INTO `signup_commission` (`id`, `referrer_id`, `referrer_name`, `referral_id`, `referral_name`, `referral_fullname`, `commission`, `date_created`, `date_updated`) VALUES
(3, 'e809ed5b38535157bf9a163fa1225ade', 'admin', 'a88b0fa765f0307f4cceec6e6dc37875', 'comp', 'Company Account', '594', '2019-12-31 02:44:06', '2019-12-31 02:44:06'),
(6, 'a88b0fa765f0307f4cceec6e6dc37875', 'comp', '956d4c4f9e38455e5ee9a27fe120df52', 'abc', 'abc', '594', '2019-12-31 03:09:11', '2019-12-31 03:09:11'),
(7, '956d4c4f9e38455e5ee9a27fe120df52', 'abc', '299d7e404a446ae1802106b127cb48f8', 'bbc', 'bbc', '594', '2019-12-31 03:13:02', '2019-12-31 03:13:02'),
(8, '299d7e404a446ae1802106b127cb48f8', 'bbc', '1572ebd58677e36834d515bf905dea81', 'Ccc', 'Ccc', '594', '2019-12-31 12:35:21', '2019-12-31 12:35:21'),
(9, 'a88b0fa765f0307f4cceec6e6dc37875', 'comp', 'fc8c4a01eaa112adc5071fdfceb09fc4', 'mike123', 'mike123', '594', '2020-01-08 02:35:39', '2020-01-08 02:35:39'),
(10, 'a88b0fa765f0307f4cceec6e6dc37875', 'comp', 'ac2666dcd4e996265cc03e1f9f4984ed', 'mimi', 'mimi', '594', '2020-01-08 02:40:19', '2020-01-08 02:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_point`
--

CREATE TABLE `transfer_point` (
  `id` int(11) NOT NULL,
  `send_uid` varchar(100) NOT NULL,
  `send_name` varchar(100) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `commission` varchar(255) NOT NULL,
  `receive_name` varchar(100) NOT NULL,
  `receive_uid` varchar(100) NOT NULL,
  `create_date` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transfer_point`
--

INSERT INTO `transfer_point` (`id`, `send_uid`, `send_name`, `amount`, `commission`, `receive_name`, `receive_uid`, `create_date`, `status`) VALUES
(5, 'e809ed5b38535157bf9a163fa1225ade', 'admin', '10000', '', 'comp', 'a88b0fa765f0307f4cceec6e6dc37875', '2019-12-31 02:44:58.832', 'RECEIVED'),
(8, 'a88b0fa765f0307f4cceec6e6dc37875', 'comp', '10000', '', 'abc', '956d4c4f9e38455e5ee9a27fe120df52', '2019-12-31 03:12:18.577', 'RECEIVED'),
(9, '956d4c4f9e38455e5ee9a27fe120df52', 'abc', '9500', '500', 'bbc', '299d7e404a446ae1802106b127cb48f8', '2019-12-31 03:18:50.518', 'RECEIVED'),
(10, '299d7e404a446ae1802106b127cb48f8', 'bbc', '10906', '574', 'abc', '956d4c4f9e38455e5ee9a27fe120df52', '2019-12-31 03:20:20.102', 'RECEIVED'),
(11, '1572ebd58677e36834d515bf905dea81', 'Ccc', '950', '50', 'Bbc', '299d7e404a446ae1802106b127cb48f8', '2019-12-31 12:36:24.955', 'RECEIVED');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) DEFAULT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `epin` char(64) DEFAULT NULL,
  `salt_epin` char(64) DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT '1',
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT '0',
  `login_type` int(2) NOT NULL DEFAULT '1' COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT '1' COMMENT '0 = admin, 1 = normal user',
  `downline_accumulated_points` decimal(50,0) NOT NULL DEFAULT '0' COMMENT 'RM1 = 100 point',
  `can_send_newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `is_referred` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `address` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `picture_id` varchar(50) DEFAULT NULL,
  `register_downline_no` varchar(255) DEFAULT '0',
  `bonus` varchar(255) DEFAULT '0' COMMENT 'register points',
  `final_amount` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT '0' COMMENT 'shipping points'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `country_id`, `full_name`, `epin`, `salt_epin`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `login_type`, `user_type`, `downline_accumulated_points`, `can_send_newsletter`, `is_referred`, `date_created`, `date_updated`, `address`, `birthday`, `gender`, `picture_id`, `register_downline_no`, `bonus`, `final_amount`, `point`) VALUES
('1572ebd58677e36834d515bf905dea81', 'Ccc', 'fghgdllghhhj@com', 'c85f05ff5382cefae902580e149ddd6c7f26ca2431b4206c0d9a26725bf47d3b', '067725a679ace1d444904ffb3ff89741ee1cb42d', NULL, NULL, NULL, 'Ccc', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-12-31 12:35:21', '2019-12-31 12:36:24', NULL, NULL, NULL, NULL, '0', '0', NULL, '980'),
('299d7e404a446ae1802106b127cb48f8', 'bbc', 'bbc@gmail.com', '433735f996fbe25fffac261d317078cabce1f65476fa97e7eadb391468425801', '7eb1a3adaefbc55793f138ddef7031ddbe1b93b4', NULL, NULL, NULL, 'bbc', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-12-31 03:13:02', '2019-12-31 12:36:24', NULL, NULL, NULL, NULL, '1', '8020', NULL, '1544'),
('42e341ef617be48f9aa9ce45be2c4274', 'adminQLM', NULL, '0557ac503411d00e75903644cfec60f73bdcaf3eb5ed71e1a146b2f5e1a68931', 'd0041bd015fc323fb09bd3a6c665cd77a33a0ceb', NULL, NULL, NULL, 'ADMINadmin', NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, 0, '2019-12-18 06:54:44', '2019-12-31 02:19:58', NULL, NULL, NULL, NULL, '0', '0', NULL, '0'),
('956d4c4f9e38455e5ee9a27fe120df52', 'abc', 'likkit1990@gmail.com', '48c2723526c10cdd43e3ed7019f139980c2b6ff88457e32929ca3b55001bee52', '6ed02e729e904f8a81a516c1d6f8b03513c2d204', NULL, NULL, NULL, 'abc', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-12-31 03:09:11', '2019-12-31 03:21:28', NULL, NULL, NULL, NULL, '1', '0', NULL, '13480'),
('a88b0fa765f0307f4cceec6e6dc37875', 'comp', 'omp@gg.cc', 'a15dc91b685e3d5c56157e22b978d55e5a7c9fb74029893d9a8f8cb8f3ca0250', '0ba0c75314ebfa3bc1100edf06a72e799d66ff5c', NULL, NULL, NULL, 'Company Account', NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, 1, '2019-12-31 02:44:06', '2020-01-08 02:40:19', NULL, NULL, NULL, NULL, '1', '82080', NULL, '91782'),
('ac2666dcd4e996265cc03e1f9f4984ed', 'mimi', 'michaelwong.vidatech@gmail.com', '26c3cd495f0e9760826024ade4ffd2b40c8ec2d6ec0201c5f5b2abca3a1300c0', 'a121ef1d7d9dc04961e6add3dbd2ae7c1936957e', NULL, NULL, NULL, 'mimi', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2020-01-08 02:40:19', '2020-01-08 02:40:19', NULL, NULL, NULL, NULL, '0', '0', NULL, '1980'),
('e809ed5b38535157bf9a163fa1225ade', 'admin', NULL, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, NULL, NULL, 'Admin', NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, 0, '2019-10-08 06:08:10', '2019-12-31 02:45:08', NULL, NULL, NULL, NULL, '0', '0', NULL, '0'),
('fc8c4a01eaa112adc5071fdfceb09fc4', 'mike123', 'likkit1990@hotmail.com', '9d48fd2049fce021841b4e8d8cd4558e2c32bed36ccf54c7be905f5ea3454153', '13cf45db724018e230e131370a642e793e5932f5', NULL, NULL, NULL, 'mike123', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2020-01-08 02:35:39', '2020-01-08 02:35:39', NULL, NULL, NULL, NULL, '0', '0', NULL, '1980');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdReferralHistory_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdReferralHistory_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdReferralHistory_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `register_point`
--
ALTER TABLE `register_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signup_commission`
--
ALTER TABLE `signup_commission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer_point`
--
ALTER TABLE `transfer_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `countryIdUser_relateTo_countryId` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `register_point`
--
ALTER TABLE `register_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `signup_commission`
--
ALTER TABLE `signup_commission`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `transfer_point`
--
ALTER TABLE `transfer_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD CONSTRAINT `referralIdReferralHistory_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `topReferrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
