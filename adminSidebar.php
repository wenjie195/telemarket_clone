<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu"> 
            <div class="float-left left-logo-div">
               <a href="adminDashboard.php" class="opacity-hover menu-a white-text"><img src="img/logo.png" class="logo-a" alt="Logo" title="Logo"></a>
            </div>



            <div class="right-menu-div float-right">
            	<a href="addReferee.php" class="opacity-hover menu-a menu-margin-right web-a">
                	Create Account
                </a>
             	<a href="viewTeleList.php" class="opacity-hover menu-a menu-margin-right web-a">
                	Staff List
                </a>
             	<a href="checkLog.php" class="opacity-hover menu-a menu-margin-right web-a">
                	Customer List
                </a>  
                <a href="adminCompany.php" class="opacity-hover menu-a menu-margin-right web-a">
                	Update Company
                </a>
             	<a href="adminStatusReason.php" class="opacity-hover menu-a menu-margin-right web-a">
                	Update Status
                </a>  
             	<a href="uploadExcel.php" class="opacity-hover menu-a menu-margin-right web-a">
                	Data Import
                </a>    
             	<a href="emer.php" class="opacity-hover menu-a menu-margin-right web-a">
                	Emergency Lockdown
                </a> 
             	<a href="logout.php" class="opacity-hover menu-a web-a">
                	Logout
                </a>
        <div id="dl-menu" class="dl-menuwrapper">
            <button class="dl-trigger">Open Menu</button>
            <ul class="dl-menu">
                <a href="addReferee.php">
                    <li class="sidebar-li hover1">
                         <p>Create Account</p>
                    </li>
                </a>
                <a href="viewTeleList.php">
                    <li class="sidebar-li hover1">
                         <p>Staff List</p>
                    </li>
                </a>
                <a href="checkLog.php">
                    <li class="sidebar-li hover1">
                         <p>Customer List</p>
                    </li>
                </a>
                <a href="uploadExcel.php">
                    <li class="sidebar-li hover1">         
                         <p>Import Data</p>
                    </li>
                </a>
                <a href="emer.php">
                    <li class="sidebar-li hover1">         
                         <p>Emergency Lockdown</p>
                    </li>
                </a>                
                <a href="logout.php">
                    <li class="sidebar-li hover1"> 
                         <p>Logout</p>
                    </li>
               </a>
            </ul>
    	</div>                
                
                                                                                            
             </div>  

                                       	
            </div>
        </div>

</header>
