<?php
//class User {
class TimeTeleUpdate {
    /* Member variables */
    // var $id, $name, $phone, $email, $status, $remark, $lastUpdated ,$noOfCall, $updateStatus, $dateCreated, $dateUpdated;
    var $id,$uid,$teleName,$customerName,$customerPhone,$updateStatus,$updateRemark,$companyName,$type,$reason,$recording,
            $remarkB,$occupation,$hobby,$location,$noOfUpdate,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
            
    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getTeleName()
    {
        return $this->teleName;
    }

    /**
     * @param mixed $teleName
     */
    public function setTeleName($teleName)
    {
        $this->teleName = $teleName;
    }

    /**
     * @return mixed
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @param mixed $customerName
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }

    /**
     * @return mixed
     */
    public function getCustomerPhone()
    {
        return $this->customerPhone;
    }

    /**
     * @param mixed $customerPhone
     */
    public function setCustomerPhone($customerPhone)
    {
        $this->customerPhone = $customerPhone;
    }

    /**
     * @return mixed
     */
    public function getUpdateStatus()
    {
        return $this->updateStatus;
    }

    /**
     * @param mixed $updateStatus
     */
    public function setUpdateStatus($updateStatus)
    {
        $this->updateStatus = $updateStatus;
    }

    /**
     * @return mixed
     */
    public function getUpdateRemark()
    {
        return $this->updateRemark;
    }

    /**
     * @param mixed $updateRemark
     */
    public function setUpdateRemark($updateRemark)
    {
        $this->updateRemark = $updateRemark;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param mixed $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return mixed
     */
    public function getRecording()
    {
        return $this->recording;
    }

    /**
     * @param mixed $recording
     */
    public function setRecording($recording)
    {
        $this->recording = $recording;
    }

    /**
     * @return mixed
     */
    public function getRemarkB()
    {
        return $this->remarkB;
    }

    /**
     * @param mixed $remarkB
     */
    public function setRemarkB($remarkB)
    {
        $this->remarkB = $remarkB;
    }

    /**
     * @return mixed
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * @param mixed $occupation
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;
    }

    /**
     * @return mixed
     */
    public function getHobby()
    {
        return $this->hobby;
    }

    /**
     * @param mixed $hobby
     */
    public function setHobby($hobby)
    {
        $this->hobby = $hobby;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getNoOfUpdate()
    {
        return $this->noOfUpdate;
    }

    /**
     * @param mixed $noOfUpdate
     */
    public function setNoOfUpdate($noOfUpdate)
    {
        $this->noOfUpdate = $noOfUpdate;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getTimeTeleUpdate($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","tele_name","customer_name","customer_phone","update_status","update_remark","company_name",
                            "type","reason","recording","remark_two","occupation","hobby","location","no_of_update","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"time_teleupdate");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id, $name, $phone, $email, $status, $remark, $lastUpdated ,$noOfCall, $updateStatus, $dateCreated, $dateUpdated);

        $stmt->bind_result($id,$uid,$teleName,$customerName,$customerPhone,$updateStatus,$updateRemark,$companyName,$type,$reason,$recording,
                                $remarkB,$occupation,$hobby,$location,$noOfUpdate,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new TimeTeleUpdate;
            $class->setId($id);
            $class->setUid($uid);
            $class->setTeleName($teleName);
            $class->setCustomerName($customerName);
            $class->setCustomerPhone($customerPhone);
            $class->setUpdateStatus($updateStatus);
            $class->setUpdateRemark($updateRemark);
            $class->setCompanyName($companyName);
            $class->setType($type);
            $class->setReason($reason);
            $class->setRecording($recording);

            $class->setRemarkB($remarkB);
            $class->setOccupation($occupation);
            $class->setHobby($hobby);
            $class->setLocation($location);

            $class->setNoOfUpdate($noOfUpdate);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
