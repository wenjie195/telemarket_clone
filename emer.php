<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Emgcsos.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$linkWeb = getEmgc($conn," WHERE type = 1 ");
$linkDetails = $linkWeb[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Account Creation | adminTele" />
    <title>Emergency Lockdown | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="width100 same-padding menu-distance min-height">
    <!-- <h1 class="h1-title">GoodBye</h1>  -->
    <h1 class="h1-title"></h1> 
    <div class="width100 text-center">
    	<img src="img/emergency.png" class="emer text-center" alt="Emergency Lockdown" title="Emergency Lockdown">
    </div>
    <div class="clear"></div>
        <form   action="utilities/selfDesFunction.php" method="POST">
            <table class="edit-profile-table">
                <tr class="profile-tr">
                    <input id="update_id" type="hidden" value="<?php echo $linkDetails->getId();?>" name="update_id" readonly>
                </tr>
            </table>

            <!-- <button class="clean red-btn margin-top30 fix300-btn" name="refereeButton">Self Destruct</button> -->
            <button class="clean red-btn margin-top30 fix300-btn" name="refereeButton">Emergency Lockdown</button>
            
        </form>

       
</div>
<style>
.emer-li{
	color:#bf1b37;
	background-color:white;}
.emer-li .hover1a{
	display:none;}
.emer-li .hover1b{
	display:block;}
</style>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "成功注册新用户！";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "注册新用户失败！";
        }
        
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("通告 !! ","'.$messageType.'");
    </script>
    ';   
}
?>
<?php include 'js.php'; ?>
</body>
</html>