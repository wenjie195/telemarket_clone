<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$customerAmount = getCustomerDetails($conn);

$goodCustomer = getCustomerDetails($conn," WHERE type = 'Right Party Contact (RPC)' ");

$badCustomer = getCustomerDetails($conn," WHERE type = 'Bad Customer' ");

// $noCallCustomerAmount = getCustomerDetails($conn," WHERE no_of_call = ? ",array("no_of_call"),array(0),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Admin Dashboard | adminTele" />
    <title>Admin Dashboard | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="width100 same-padding menu-distance min-height">
    <h1 class="h1-title open">Admin Dashboard</h1>

    <div class="clear"></div>
    <div class="width100">
        <a href="checkLog.php">
            <div class="three-div opacity-hover white-div-with-shadow">
                <img src="img/customer-list.png" class="big-icon" alt="Total Customer" title="Total Customer">
                <p class="big-p">Total Customer</p>

                <?php
                if($customerAmount)
                {   
                    $totalCustomer = count($customerAmount);
                }
                else
                {   $totalCustomer = 0;   }
                ?>

                <!-- <p class="small-p">50,000</p> -->
                <p class="small-p"><?php echo $totalCustomer;?></p>
            </div>
        </a>
        <a href="checkLogGood.php">
            <div class="three-div mid-three-div opacity-hover white-div-with-shadow">
                <img src="img/right-contact.png" class="big-icon"  alt="Customer - Right Party Contact" title="Customer - Right Party Contact">
                <p class="big-p">Customer<br>Right Party Contact</p>

                <?php
                if($goodCustomer)
                {   
                    $totalGoodCustomer = count($goodCustomer);
                }
                else
                {   $totalGoodCustomer = 0;   }
                ?>

                <!-- <p class="small-p">5,000</p> -->
                <p class="small-p"><?php echo $totalGoodCustomer;?></p>
            </div>
        </a>
        <a href="checkLogBlack.php">
            <div class="three-div opacity-hover white-div-with-shadow">
                <img src="img/no-contact.png" class="big-icon hover1a"  alt="Customer - No Contact" title="Customer - No Contact">
                <p class="big-p">Customer<br>No Contact</p>

                <?php
                if($badCustomer)
                {   
                    $totalBadCustomer = count($badCustomer);
                }
                else
                {   $totalBadCustomer = 0;   }
                ?>

                <!-- <p class="small-p">45,000</p> -->
                <p class="small-p"><?php echo $totalBadCustomer;?></p>
            </div>
        </a>    
   
	</div>
    <div class="clear"></div>

        
</div>
<style>
.dashboard-li{
	color:#bf1b37;
	background-color:white;}
.dashboard-li .hover1a{
	display:none;}
.dashboard-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>
</body>
</html>