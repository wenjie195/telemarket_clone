<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/CompanySelection.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];
$companyDetails = getCompanySelection($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Login | adminTele" />
    <title>Login | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
    <?php include 'autolog.php' ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="width100 overflow">

	<div class="width50 grey-bg right-grey-div">
    	<h1 class="tele-h1">Company Selection</h1>
        <p class="login-p margin-bottom">
        	Please select a Company.
        </p>
        <form  class="tele-form" action="utilities/companySelectionFunction.php" method="POST">
            <div class="input50-div">
                <select class="clean tele-input" id="company_name" value="<?php echo $companyDetails[0]->getCompanyName();?>" name="company_name">
                    <option value="">Please Select a Company</option>
                    <?php
                    for ($cntPro=0; $cntPro <count($companyDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $companyDetails[$cntPro]->getCompanyName(); 
                    ?>"> 
                    <?php 
                    echo $companyDetails[$cntPro]->getCompanyName(); //take in display the options
                    ?>
                    </option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <button class="clean red-btn"name="selectBtn">Select</button>
         </form>
    </div>
	<div class="width50 left-red-div red-bg">
    	<img src="img/login.png" class="login-img" alt="Login" title="Login">
    </div>    
</div>

<?php include 'js.php'; ?>
</body>
</html>