<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Reason.php';
require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE user_type =1 ");

$statusDetails = getStatus($conn);
$reasonDetails = getReason($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Status and Reason | adminTele" />
    <title>Update Status | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="width100 same-padding menu-distance min-height">

    <h1 class="h1-title">Status</h1>

    <div class="clear"></div>

    <div class="width100 extra-m-btm">
    	<button class="clean red-btn margin-top30 fix300-btn add-status-btn text-center open-status">Update Status</button>
    </div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>STATUS</th>
                            <th>REASON</th>
                            <th>DATE CREATED</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php

                        if($reasonDetails)
                        {   
                            for($cnt = 0;$cnt < count($reasonDetails) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $reasonDetails[$cnt]->getStatus();?></td>
                                <td><?php echo $reasonDetails[$cnt]->getReasonA();?></td>
                                <td><?php echo $reasonDetails[$cnt]->getDateCreated();?></td>
                            <?php
                            }?>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>

                </table>
            </div>
    </div>
</div>
<style>
.statusreason-li{
	color:#bf1b37;
	background-color:white;}
.statusreason-li .hover1a{
	display:none;}
.statusreason-li .hover1b{
	display:block;}
</style>
<?php include 'js.php'; ?>
</body>
</html>