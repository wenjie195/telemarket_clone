<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/CompanySelection.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';


function addNewCompany($conn,$company)
{
     if(insertDynamicData($conn,"companyselection",array("company_name"),
     array($company),"s") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $company = rewrite($_POST["company_name"]);

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $status."<br>";

     if(addNewCompany($conn,$company))
     {
          header('Location: ../adminCompany.php?type=1');
     }
     else
     {
          header('Location: ../adminCompany.php?type=2');
     }
}
else
{
     header('Location: ../index.php');
}
?>