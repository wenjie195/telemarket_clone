<?php
// if (session_id() == ""){
//      session_start();
//  }
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    $id = $_POST["id"];
    $productName = $_POST["product_name"];
    $productStock = $_POST["product_stock"];
    $productPrice = $_POST["product_price"];
    $productDescription = $_POST["product_description"];

    $productImages = rewrite($_POST["uploadfile"]);

    $name = $_FILES['file']['name'];
    $target_dir = "../ProductImages/";
    $target_file = $target_dir . basename($_FILES["file"]["name"]);
    //$image_text = mysqli_real_escape_string($conn, $_POST['number']);


    // Select file type
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    // Valid file extensions
    $extensions_arr = array("jpg","jpeg","png","gif");

    if( in_array($imageFileType,$extensions_arr) ){


     move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$name);


    //  header('Location: ../index.php');
}

}

if(isset($_POST['editSubmit']))
{
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($productName)
    {
        array_push($tableName,"name");
        array_push($tableValue,$productName);
        $stringType .=  "s";
    }
    if($productStock)
    {
        array_push($tableName,"stock");
        array_push($tableValue,$productStock);
        $stringType .=  "i";
    }if($productPrice)
    {
        array_push($tableName,"price");
        array_push($tableValue,$productPrice);
        $stringType .=  "i";
    }if($productDescription)
    {
        array_push($tableName,"description");
        array_push($tableValue,$productDescription);
        $stringType .=  "s";
    }
    if($name)
    {

      move_uploaded_file($_FILES['file']['tmp_name'],$target_file);

        array_push($tableName,"images");
        array_push($tableValue,$name);
        $stringType .=  "s";
    //}
  }

    array_push($tableValue,$id);
    $stringType .=  "i";
    $withdrawUpdated = updateDynamicData($conn,"product"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../adminProduct.php?type=1');
    }
    else
    {
        echo "fail";

    }
}
else
{
  //  echo "dunno";

}

  if( isset($_POST['deleteProduct']) )
  	{
  		$id = $_POST['id'];
  		$sql= "UPDATE product SET display=0 WHERE id=$id";

if ($conn->query($sql) === TRUE) {
  $_SESSION['messageType'] = 1;
  header('Location: ../adminProduct.php?type=2');
} else {
  $_SESSION['messageType'] = 1;
  header('Location: ../adminProduct.php?type=3');
}

  	}

    if( isset($_POST['restoreSubmit']) )
    	{
    		$id = $_POST['id'];
    		$sql= "UPDATE product SET display=1 WHERE id=$id";

  if ($conn->query($sql) === TRUE) {
    $_SESSION['messageType'] = 1;
    header('Location: ../adminProduct.php?type=2');
  } else {
    $_SESSION['messageType'] = 1;
    header('Location: ../adminProduct.php?type=3');
  }

    	}



?>
